FROM centos:7

RUN yum install -y wget vim

# centos is piece of shit
RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm ; yum -y install git2u-all

RUN wget https://github.com/mikefarah/yq/releases/download/3.1.0/yq_linux_amd64 -O /usr/local/bin/yq ; chmod +x /usr/local/bin/yq

# argocd from our local argocd should be safe
RUN wget --no-check-certificate https://argocd.demo-whatever.net/download/argocd-linux-amd64 -O /usr/local/bin/argocd ; chmod +x /usr/local/bin/argocd

# lets go uprivileged for deploying :)
RUN useradd -m proton
USER proton
